module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],

  pwa: {
    name: 'TrackReps',
    themeColor: '#ffc107',
    msTileColor: '#ffc107',
    manifestOptions: {
      background_color: 'white'
    }
  }
}