export default {
  app: {
    title: "Tuas Repetições Diárias",
    subtitle: "Mantenha seu ser em forma",
    actions: {
      close: "Fechar"
    }
  },
  menu: {
    cards: "Itens",
    newCard: "Novo Item",
    removeCard: "Remover Item",
    reorderCards: "Reordenar Itens",
    undoRemovedCard: "Desfazer",
    exportCards: "Exportar",
    importCards: "Importar",
    resetAllCards: "Zerar Todas os Itens",
    languages: "Língua",
    cardTextFontSize: "Tamanho da Fonte",
    shareCard: "Compartilhar Item",
  },
  card: {
    countTitle: "Qtd: {count}",
    days: "dias",
    dateStarted: "Data de Início",
    goalLabel: "Objetivo do Dia",
    countPlus: "Mais +1",
    countReset: "Zerar Contagem",
    countMinus: "Menos -1",
    previousCard: "Anterior",
    nextCard: "Próximo"
  },
  cardForm: {
    save: "Salvar",
    cancel: "Cancelar",
    welcomeMessage:"Bem Vindo! Crie agora seu primeiro item!",
  },
  cardReordering: {
    sendDown: "Para Baixo",
    sendUp: "Para Cima"
  },
  backupImport: {
    title: "Importar Backup",
    subtitle: "Selecione o arquivo de backup para importar",
    importSuccess: "Itens importadas com sucesso!",
    importError: "Arquivo de backup inválido",
  },
  messages: {
    removedCard: "Removido",
    allDone: "Parabéns, todos concluídos!",
    noCardsAvailable: "Nenhum Item Disponível",
  },
  demoItems: {
    demoItem1:`Bem Vindo!
    Esta é sua primeira visita, pelo menos neste dispositivo ;-)

    Clique 3 vezes no botão com sinal de + logo abaixo para eu te levar para o próximo item automaticamente.`,
    demoItem2:`Parabéns!
    Agora você já sabe como eu posso te ajudar a realizar as tuas repetições diárias.

    Para finalizar, clique 2 vezes no botão com sinal de + para comemorar!`,
  }
};
