import Vue from 'vue';
import VueI18n from 'vue-i18n';

import ptBRFile from './ptBR';
import enUSFile from './enUS';

Vue.use(VueI18n);

const langs = { 
    ptBR: 'pt-BR', 
    enUS: 'en-US', 
};

const locale = localStorage.getItem('lang') ? localStorage.getItem('lang') : navigator.language;
localStorage.setItem("lang", locale);

const i18n = new VueI18n({
    locale: locale,
    messages: {
        'pt-BR': ptBRFile,
        'en-US': enUSFile,
    }
});

export { i18n, langs };