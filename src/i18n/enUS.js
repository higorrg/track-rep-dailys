export default {
  app: {
    title: "TrackRepDailys",
    subtitle: "Keep tracking your repetitions",
    actions: {
      close: "close"
    }
  },
  menu: {
    cards: "Items",
    newCard: "New Item",
    removeCard: "Remove Item",
    reorderCards: "Reorder Items",
    undoRemovedCard: "Undo",
    exportCards: "Export",
    importCards: "Import",
    resetAllCards: "Reset All Items",
    languages: "Languages",
    cardTextFontSize: "Font Size",
    shareCard: "Share Item",
  },
  card: {
    countTitle: "Count: {count}",
    days: "days",
    dateStarted: "Date Started",
    goalLabel: "Goal fo the Day",
    countPlus: "Count +1",
    countReset: "Reset Counting",
    countMinus: "Count -1",
    previousCard: "Previous",
    nextCard: "Next"
  },
  cardForm: {
    save: "Save",
    cancel: "Cancel",
    welcomeMessage:"Welcome! Create your first Item righ now!",
  },
  cardReordering: {
    sendDown: "Send Down",
    sendUp: "Send Up"
  },
  backupImport: {
    title: "Backup Import",
    subtitle: "Select the backup file to import",
    importSuccess: "Items successfully imported!",
    importError: "Invalid backup file"
  },
  messages: {
    removedCard: "Removed",
    allDone: "Well Done!",
    noCardsAvailable: "No Cards Available",
  },
  demoItems: {
    demoItem1:`Welcome!
    This is your first visit, at least on this device ;-)
    
    Click 3 times on the button with the + sign below it to automatically take you to the next item.`,
    demoItem2:`Congratulations!
    Now you know how I can help you to track your daily repetitions.

    Finally, click twice on the button with the + sign to celebrate!`,
  }
};
