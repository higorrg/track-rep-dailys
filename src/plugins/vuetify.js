import Vue from "vue";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";

Vue.use(Vuetify);

const opts = {
  theme: {
    themes: {
      light: {
        primary: "#ffc107",
        secondary: "#607d8b",
        accent: "#ffeb3b",
        error: "#f44336",
        warning: "#cddc39",
        info: "#00bcd4",
        success: "#8bc34a"
      }
    }
  }
};

export default new Vuetify(opts);
