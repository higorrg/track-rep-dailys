import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import "./registerServiceWorker";
import { i18n } from "./i18n/config";
import VueSweetalert2 from "vue-sweetalert2";
import "sweetalert2/dist/sweetalert2.min.css";
import VueConfetti from "vue-confetti";
import router from './router'

Vue.use(VueConfetti);
Vue.use(VueSweetalert2);

Vue.config.productionTip = false;

Vue.filter("formatDate", function(value) {
  if (!value) return null;
  return new Date(value).toLocaleDateString(i18n.locale);
});

Vue.filter("formatDatePicker", function(value) {
  if (!value) return null;
  try {
    const [year, month, day] = value.split("-");
    return new Date(month + "-" + day + "-" + year).toLocaleDateString(
      i18n.locale
    );
  } catch (e) {
    return value;
  }
});

new Vue({
  vuetify,
  i18n,
  router,
  render: h => h(App)
}).$mount("#app");
