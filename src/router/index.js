import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/card-item',
    name: 'CardItem',
    component: () => import(/* webpackChunkName: "CardItem" */ '../views/CardItem.vue')
  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/terms-and-conditions',
    name: 'TermsAndConditions',
    component: () => import(/* webpackChunkName: "TermsAndConditions" */ '../views/TermsAndConditions.vue')
  },
  {
    path: '/reorder-cards',
    name: 'ReorderCards',
    component: () => import(/* webpackChunkName: "ReorderCards" */ '../views/ReorderCards.vue')
  },
  {
    path: '/import-items',
    name: 'ImportItems',
    component: () => import(/* webpackChunkName: "ImportItems" */ '../views/ImportItems.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
